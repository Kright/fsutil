You can use this source code for any purpose

Program recursively walks for all files, compares them by extension, size and sha256 hash (for files less then 10Mb).

Program deletes files in two cases:

1. duplicated files are in same directory, file with the shortest name wins
2. files in directory and subdirectory, subdirectory wins (for example, photos/2020 and photos/2020/01)

Program has fake mode, when it doesn't delete files.