package com.gitlab.kright

import java.io.File

@main
def main(args: String*): Unit = {
  println(s"args = ${args}")

  val root = new File("..")

  val files = root.recursiveListFiles()
  println(s"total files: ${files.size}")
  val doubles = FileInfo.findDoubles(files.map(FileInfo(_)))

  println(s"doubles count = ${doubles.size}:")
  val deleter: FileRemover = FileRemover(debug = true, fake = true)
  doubles.foreach(dd => deleter.removeAllExceptOne(dd))
}


extension (b: FileRemover)
  def removeAllExceptOne(dd: Iterable[FileInfo]): Unit = {
    if (deleteInSameDirectory(dd) || deleteLessSpecificPaths(dd)) return
    println(s"don't know what to delete: ${dd.mkString(",")}")
  }

  private def deleteInSameDirectory(files: Iterable[FileInfo]): Boolean = {
    val parent = files.head.file.getParent
    if (!files.tail.forall(_.file.getParent == parent)) {
      return false
    }

    val best = files.minBy(_.file.getName.size)
    val others = files.filter(_ != best)
    println(s"common parent = ${best.file.getParent} for ${best}, remove others = ${others.mkString(", ")}")
    others.foreach(f => b.delete(f.file))
    return true
  }

  private def deleteLessSpecificPaths(dd: Iterable[FileInfo]): Boolean = {
    getMostSpecificParentPath(dd) match {
      case Some(path) => {
        val (ok, others) = dd.partition(_.file.getParentFile.getAbsolutePath == path)
        println(s"oks = ${ok.mkString(", ")} delete others in less specific directory = ${others.mkString(", ")}")
        others.foreach(f => b.delete(f.file))
        deleteInSameDirectory(ok)
        true
      }
      case None => false
    }
  }

private def getMostSpecificParentPath(files: Iterable[FileInfo]): Option[String] =
  val paths = files.map(_.file.getParentFile.getAbsolutePath)
  val longestPath = paths.maxBy(_.length)
  if (paths.forall(p => longestPath.startsWith(p)))
    Some(longestPath)
  else
    None

