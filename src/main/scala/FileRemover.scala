package com.gitlab.kright

import java.io.File

trait FileRemover:
  def delete(f: File): Unit

object FileRemover:
  def apply(debug: Boolean, fake: Boolean): FileRemover =
    new FileRemover:
      override def delete(f: File): Unit =
        if (debug) println(s"attempt to delete ${f}")
        if (!fake) f.delete()