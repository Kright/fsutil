package com.gitlab.kright

import java.io.File
import java.nio.file.Files

extension (f: File)
  def recursiveListFiles(): Array[File] =
    if (f.isFile) {
      Array(f)
    } else {
      require(f.isDirectory)
      f.listFiles().flatMap(_.recursiveListFiles())
    }
    
  def bytes: Array[Byte] =
    require(f.isFile)
    require(f.canRead)
    Files.readAllBytes(f.toPath)
