package com.gitlab.kright

import java.io.File
import java.security.MessageDigest
import scala.util.matching.Regex

class FileInfo(val file: File) {
  val fileSize = file.length()
  val extension = file.getName.split(Regex.quote(".")).last.toLowerCase

  override def toString: String = s"FileInfo(${file}, fileSize=${fileSize})"
}

object FileInfo:
  def findDoublesByMeta(list: Iterable[FileInfo]): Iterable[Iterable[FileInfo]] =
    list.groupBy(i => (i.fileSize, i.extension)).map(_._2).filter(_.size > 1)

  def findDoublesBySHA256(list: Iterable[FileInfo], sizeThreshold: Int = 10000000): Iterable[Iterable[FileInfo]] =
    val md = MessageDigest.getInstance("SHA-256")
    list.groupBy{ f =>
      if (f.fileSize < sizeThreshold) {
        md.digest(f.file.bytes).map("%02x".format(_)).mkString
      } else {
        f.fileSize.toString
      }
    }.map(_._2).filter(_.size > 1)

  def findDoubles(list: Iterable[FileInfo]): Iterable[Iterable[FileInfo]] =
    findDoublesByMeta(list).flatMap(ll => findDoublesBySHA256(ll))
